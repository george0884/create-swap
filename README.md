# create-swap
Para crear archivo swap fácilmente mediante menú
```
  SCRIPT PARA CREAR ARCHIVO SWAP 
  SELECCIONA UNA OPCIÓN: 
 1.-Crear swap.
 2.-Activar swap.
 3.-Eliminar y desactivar swap.
 4.-Corroborar estado de swap.
 5.-Optimizar swappiness
 6.-Reiniciar sistema
 7.-Apagar sistema

 ```
 ```
 git clone https://github.com/george0884/create-swap.git && cd create-swap && sudo chmod +x create-swap-menu.sh && sudo ./create-swap-menu.sh
 ```
``` 
git clone https://github.com/george0884/create-swap.git

cd create-swap

$ sudo chmod +x create-swap-menu.sh

$ sudo ./create-swap-menu.sh
```
![Captura de pantalla de 2019-05-13 05-22-23](https://user-images.githubusercontent.com/11846298/57606724-de088300-753f-11e9-8328-72e8c3c9182e.png)
![Captura de pantalla de 2019-05-13 05-20-50](https://user-images.githubusercontent.com/11846298/57606753-f4164380-753f-11e9-8700-9a10483ca591.png)
![Captura de pantalla de 2019-05-13 16-42-48](https://user-images.githubusercontent.com/11846298/57653022-40489e80-75a7-11e9-9a0c-f704589bba6f.png)
